import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
      title: "Contador de Pessoas",
      home: Home()));
}


class Home extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<Home> {

  int _personCount = 0;
  String _msgAccess = "Pode entrar!";

  void _changePersonCount(int v){
    setState(() {
      _personCount+= v;
      if(_personCount < 0)
      {
        _msgAccess = "Mundo Invertido!!!";
      }
      else if(_personCount <= 10)
      {
        _msgAccess = "Pode entrar!";
      }
      else
      {
        _msgAccess = "Lotado!!!";
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
        children: <Widget>[
          Image.asset("images/restaurante.jpg",
              fit: BoxFit.cover, height: 1000.0),
          Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Pessoas: $_personCount",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: FlatButton(
                        child: Text("+1",
                            style:
                                TextStyle(color: Colors.white, fontSize: 40.0)),
                        onPressed: () {
                          debugPrint("+1"); 
                          _changePersonCount(1);
                        },
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.all(10.0),
                        child: FlatButton(
                          child: Text("-1",
                              style: TextStyle(
                                  color: Colors.white, fontSize: 40.0)),
                          onPressed: () {
                            debugPrint("-1"); 
                            _changePersonCount(-1);
                          },
                        )),
                  ],
                ),
                Text(_msgAccess,
                    style: TextStyle(
                        color: Colors.white,
                        fontStyle: FontStyle.italic,
                        fontSize: 30.0)),
              ])
        ],
      );
  }
}